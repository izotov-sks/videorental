<?php

function __autoload($class)
{
    require $class . '.php';
}

$film1 = new Film('MyKino-1', Film::NORMAL);
$film2 = new Film('MyKino-2', Film::NEUE_VEROEFFENTLICHUNG);
$medium1 = new Medium('qwe321', $film1);
$medium2 = new Medium('asd123', $film2);
$medium3 = new Medium('zxc098', $film2);
$miete1 = new Miete($medium1, 1);
$miete2 = new Miete($medium3, 2);
$miete3 = new Miete($medium2, 1);
$kunde = new Kunde('Anton');
$kunde->neueMiete($miete1);
$kunde->neueMiete($miete2);
$kunde->neueMiete($miete3);

var_dump($kunde);
echo $kunde->rechnung();
echo $kunde->rechnungAlsHtml();
die();
