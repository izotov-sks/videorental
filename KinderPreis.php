<?php

class KinderPreis extends Preis
{
    public function getCharge($days)
    {
        $betrag = 0;
        $betrag += 1.5;
        if ($days > 3) {
            $betrag += ($days - 3) * 1.5;
        }
        return $betrag;
    }

    public function getPreisCode()
    {
        return Film::KINDER;
    }
}