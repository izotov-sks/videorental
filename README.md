To test this app you'll need:
* setup PHPUnit using composer
* define namespaces for classes(optional)
* move on with instructions described here:
- https://phpunit.de/manual/current/en/installation.html
- https://phpunit.de/getting-started.html
NEEDs to test(asserts):
* dependecies;
* instances of;
* datatypes(in/out);
* expected calculations' values;
* fails/oks;
* exceptions;
* objects terminations, runtime;
* classes inheritance and methods/properties visibility;

Keep in mind, that it's not final version - only best practices I was able to implement there within 2 hours;

Thanks.
