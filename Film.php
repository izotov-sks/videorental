<?php

/**
 * Class Film
 * Die Film-Klasse beinhaltet Informationen eines Films. Die Videothek kann mehrere Medien eines Films auf Lager haben.
 */
class Film implements RegistrarInterface
{

    const KINDER = 2;
    const NORMAL = 0;
    const NEUE_VEROEFFENTLICHUNG = 1;

    private $name;
    private $preis;

    /**
     * @param string $name
     * @param int    $preisCode
     */
    public function __construct($name, $preisCode)
    {
        $this->name = $name;
        $this->setPreisCode($preisCode);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPreisCode()
    {
        return $this->preis->getPreisCode();
    }

    public function setPreisCode($preisCode)
    {
        switch ($preisCode) {
            case Film::NORMAL:
                $this->preis = new NormalPreis();
                break;
            case Film::NEUE_VEROEFFENTLICHUNG:
                $this->preis = new NeueVeroeffentlichungPreis();
                break;
            case Film::KINDER:
                $this->preis = new KinderPreis();
                break;
            default:
                throw new Exception("Incorrect Preis Code");
        }
    }

    public function getPreis()
    {
        return $this->preis->getPreis();
    }

    public function getCharge($days)
    {
        return $this->preis->getCharge($days);
    }

    public function persist()
    {
        Registrar::add("Filme", $this);
    }


    /**
     * @param string $name
     *
     * @return Film
     */
    public static function get($name)
    {
        return Registrar::get("Filme", $name);
    }

} 