<?php

class NeueVeroeffentlichungPreis extends Preis
{
    public function getCharge($days)
    {
        $betrag = 0;
        $betrag += $days * 3;
        return $betrag;
    }

    public function getPreisCode()
    {
        return Film::NEUE_VEROEFFENTLICHUNG;
    }

}