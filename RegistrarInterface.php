<?php

interface RegistrarInterface
{
    public function persist();
    public static function get($name);
}