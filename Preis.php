<?php

abstract class Preis
{
    abstract function getCharge($days);
    abstract function getPreisCode();

}