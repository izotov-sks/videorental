<?php

/**
 * Class Medium
 * Die Medium-Klasse enthält ein physisches Medium eines Films
 */
class Medium
{

    /**
     * @var Film $film
     */
    protected $film;

    /**
     * @var string $serienNummer
     */
    protected $serienNummer;

    /**
     * @param string $serienNummer
     * @param Film   $film
     */
    public function __construct($serienNummer, Film $film)
    {
        $this->serienNummer = $serienNummer;
        $this->film = $film;
    }

    /**
     * @return Film
     */
    public function getFilm()
    {
        return $this->film;
    }

    public function getSerienNummer()
    {
        return $this->serienNummer;
    }
}