<?php

class NormalPreis extends Preis
{
    public function getCharge($days)
    {
        $betrag = 0;
        $betrag += $betrag + 2;
        if ($days > 2) {
            $betrag += ($days - 2) * 1.5;
        }
        return $betrag;
    }

    public function getPreisCode()
    {
        return Film::NORMAL;
    }
}