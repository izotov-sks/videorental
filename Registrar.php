<?php

/**
 * Class Registrar
 * Die Registrat-Klasse ist eine Dummy-Implementierung für eine Persistenzschicht
 */
class Registrar
{

    protected static $data = array();

    public static function add($key, $value)
    {
        if (!isset(self::$data[$key])) {
            self::$data[$key] = array();
        }

        self::$data[$key][$value->getName()] = $value;
    }

    public static function get($key, $name)
    {
        return self::$data[$key][$name];
    }

} 