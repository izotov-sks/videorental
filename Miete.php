<?php

/**
 * Class Miete
 * Die Klasse "Miete" repräsentiert die Miete eines Films durch einen Kunden
 */
class Miete
{
    /**
     * @var int $tageEntliehen
     */
    private $tageEntliehen;

    /**
     * @var Medium $medium
     */
    private $medium;

    /**
     * @param Medium $medium
     * @param int    $tageEntliehen
     */
    public function __construct(Medium $medium, $tageEntliehen)
    {
        $this->medium = $medium;
        $this->tageEntliehen = $tageEntliehen;
    }

    public function getMedium()
    {
        return $this->medium;
    }

    public function getTageEntliehen()
    {
        return $this->tageEntliehen;
    }

    /**
     * @return integer
     */
    public function getBonuses()
    {
        $bonusPunkte = 0;
        $bonusPunkte++;
        if ($this->getMedium()->getFilm()->getPreisCode(
            ) === Film::NEUE_VEROEFFENTLICHUNG && $this->getTageEntliehen() > 1
        ) {
            $bonusPunkte++;
        }
        return $bonusPunkte;
    }

    /**
     * @return float
     */
    public function getCharge()
    {
        return $this->getMedium()->getFilm()->getCharge($this->getTageEntliehen());
    }
}