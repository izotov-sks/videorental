<?php

/**
 * Class Kunde
 * Die Klasse Kunde repräsentiert einen Kunden und beinhaltet eine Methode zum Generieren einer Rechnung
 */
class Kunde implements RegistrarInterface
{
    protected $mieten = array();
    private $name;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Miete $miete
     */
    public function neueMiete(Miete $miete)
    {
        $this->mieten[] = $miete;
    }

    /**
     * @param string $name
     *
     * @return Kunde
     */
    public static function get($name)
    {
        return Registrar::get('Kunden', $name);
    }


    public function persist()
    {
        Registrar::add('Kunden', $this);
    }

    /**
     * @return string
     */
    public function rechnung()
    {
        $gesamtBetrag = 0;
        $result = "Mietrechnung für " . $this->getName() . PHP_EOL;
        foreach ($this->mieten as $miete) {
            $betrag = $miete->getCharge();
            $gesamtBetrag += $betrag;
            $bonusPunkte = $miete->getBonuses();
            $result .= "\t" . $miete->getMedium()->getFilm()->getName() . "\t" . $betrag . "\n";
        }
        $result .= 'Geschuldeter Betrag: ' . $gesamtBetrag . "\n";
        $result .= 'Sie haben ' . $bonusPunkte . ' Bonuspunkte erhalten.' . "\n";

        return $result;
    }


    /**
     * @return string
     */
    public function rechnungAlsHtml()
    {
        $gesamtBetrag = 0;
        $bonusPunkte = 0;
        $result = "<h1>Mietrechnung für " . $this->getName() . '</h1>';
        $result .= '<table>';
        foreach ($this->mieten as $miete) {
            $betrag = $miete->getCharge();
            $gesamtBetrag += $betrag;
            $bonusPunkte = $miete->getBonuses();
            $result .= '<tr><td>' . $miete->getMedium()->getFilm()->getName() . "</td><td>" . $betrag . "</td></tr>";
        }
        $result .= '</table>';
        $result .= '<h3>Geschuldeter Betrag: ' . $gesamtBetrag . "</h3>";
        $result .= '<h3>Sie haben ' . $bonusPunkte . ' Bonuspunkte erhalten.' . "</h3>";

        return $result;
    }
}